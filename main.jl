using DataFrames, CSV, Query, Match, TerminalMenus, Diana, JSON, Merly, JSONTables


macro loadFile(filePath)
    return Threads.@spawn CSV.File(string(filePath), silencewarnings=true)
end
const exercisestypesFile = @loadFile exercisestypes.csv
const exercisesFile = @loadFile exercises.csv

const exerciserepetitionsyoyo_csv = @loadFile exerciserepetitionsyoyo.csv
const exerciserepetitionscone_csv = @loadFile exerciserepetitionscone.csv
const exerciserepetitionsfreecharge_csv = @loadFile exerciserepetitionsfreecharge.csv
const exerciserepetitionsplatformimtp_csv = @loadFile exerciserepetitionsplatformimtp.csv
const exerciserepetitionsplatformdj_csv = @loadFile exerciserepetitionsplatformdj.csv
const exerciserepetitionsplatformcmj_csv = @loadFile exerciserepetitionsplatformcmj.csv
const exerciserepetitionsplatformsquat_csv = @loadFile exerciserepetitionsplatformsquat.csv
const exerciserepetitionsdynamometer_csv = @loadFile exerciserepetitionsdynamometer.csv
const exerciserepetitionsdynamometerd_csv = @loadFile exerciserepetitionsdynamometerd.csv
const exerciserepetitionsplatformhht_csv = @loadFile exerciserepetitionsplatformhht.csv
const exerciserepetitionsplatformbounce_csv =
    @loadFile exerciserepetitionsplatformbounce.csv
const exerciserepetitionsplatformsquatjump_csv =
    @loadFile exerciserepetitionsplatformsquatjump.csv

function findType(item::Symbol)::String
    @match item begin
        :RemoPrensa => "1"
        :Yoyo => "2"
        :SillonCamilla => "3"
        :EmpujeFrontal => "4"
        :HipThrust => "5"
        :ConoTR => "6"
        :PoleaConica => "7" #
        :RemoCono => "8"
        :Cone => "8"
        :CargaLibre => "9"
        :FreeCharge => "9"
        :CargaLibreCompleja => "10"
        :ComplexFreeCharge => "10"
        :IMTP => "11"
        :McCall => "11"
        :Pull => "11"
        :RSIDJ => "12"
        :SaltoContinuo => "13"
        :Bounce => "13"
        :CMJ => "14"
        :Sentadilla => "15"
        :Squat => "15"
        :Dinamometro => "16"
        :HopAndHold => "17"
        :DinamometroDoble => "18"
        :SquatJump => "19"
        _ => "14"
    end
end
function selectFile(nType::String)::Task
    @match nType begin
        "1" => exerciserepetitionsyoyo_csv
        "2" => exerciserepetitionsyoyo_csv
        "3" => exerciserepetitionsyoyo_csv
        "4" => exerciserepetitionsyoyo_csv
        "5" => exerciserepetitionsyoyo_csv
        "6" => exerciserepetitionsyoyo_csv
        "7" => exerciserepetitionsyoyo_csv
        "9" => exerciserepetitionsfreecharge_csv
        "8" => exerciserepetitionscone_csv
        "10" => exerciserepetitionsfreecharge_csv
        "11" => exerciserepetitionsplatformimtp_csv
        "12" => exerciserepetitionsplatformdj_csv
        "13" => exerciserepetitionsplatformbounce_csv
        "14" => exerciserepetitionsplatformcmj_csv
        "15" => exerciserepetitionsplatformsquat_csv
        "16" => exerciserepetitionsdynamometer_csv
        "17" => exerciserepetitionsplatformhht_csv
        "18" => exerciserepetitionsdynamometerd_csv
        "19" => exerciserepetitionsplatformsquatjump_csv
        # "exerciserepetitionsphotocell.csv"
        _ => exerciserepetitionsplatformcmj_csv
    end
end

function readFilter(file::Task, predicate::Function, property::Symbol=:NoProp)
    return fetch(file) |>
           @filter(predicate(_)) |>
           DataFrame |>
           x -> property === :NoProp ? x : x[!, property|>String]
end


function prt(x)
    print(x)
    return true
end


const exerciseOptions = [
    "RemoPrensa",
    "Yoyo",
    "SillonCamilla",
    "EmpujeFrontal",
    "HipThrust",
    "ConoTR",
    "PoleaCónica",
    "RemoCono",
    "CargaLibre",
    "CargaLibreCompleja",
    "IMTP",
    "McCall",
    "Pull",
    "RSIDJ",
    "SaltoContinuo",
    "CMJ",
    "Sentadilla",
    "Dinamometro",
    "HopAndHold",
    "DinamometroDoble",
    "SquatJump",
]
# TYPE = request("Seleccione Ejercicio:", exerciseMenu) |> n -> findType(exerciseOptions[n])
# USER = 233 # prt("USER ID:  ") && parse(Int64, readline())
# const exerciseMenu = RadioMenu(exerciseOptions, pagesize=10)

function queryCSV(USER::Integer, TYPEstr)
    TYPE = findType(TYPEstr)
    repetitionsFile = selectFile(TYPE)
    return readFilter(
        exercisestypesFile,
        x -> x.idEvaluationType === TYPE,
        :idExerciseTypeGlobal,
    ) |>
           TYPELIST ->
        readFilter(
            exercisesFile,
            x -> x.idUser === USER && x.idExerciseType in TYPELIST,
            :idExercise,
        ) |>
        EXERCISES -> readFilter(repetitionsFile, x -> string(x.idExercise) in EXERCISES)
end

const schema = """
               type Query {
                   exercises: Exercises
               }
               type Exercises {
                   imtp: IMTP
               }
               type RemoPrensa {
                   idExercise: String
                   repetitionNumber: String
                   maxVelocity: String
                   maxAceleration: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   maxConcentricPower: String
                   maxEccentricPower: String
                   avgVelocity: String
                   avgAceleration: String
                   avgConcentricForce: String
                   avgEccentricForce: String
                   avgConcentricPower: String
                   avgEccentricPower: String
               }
               type Yoyo {
                   idExercise: String
                   repetitionNumber: String
                   maxVelocity: String
                   maxAceleration: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   maxConcentricPower: String
                   maxEccentricPower: String
                   avgVelocity: String
                   avgAceleration: String
                   avgConcentricForce: String
                   avgEccentricForce: String
                   avgConcentricPower: String
                   avgEccentricPower: String
               }
               type SillonCamilla {
                   idExercise: String
                   repetitionNumber: String
                   maxVelocity: String
                   maxAceleration: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   maxConcentricPower: String
                   maxEccentricPower: String
                   avgVelocity: String
                   avgAceleration: String
                   avgConcentricForce: String
                   avgEccentricForce: String
                   avgConcentricPower: String
                   avgEccentricPower: String
               }
               type EmpujeFrontal {
                   idExercise: String
                   repetitionNumber: String
                   maxVelocity: String
                   maxAceleration: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   maxConcentricPower: String
                   maxEccentricPower: String
                   avgVelocity: String
                   avgAceleration: String
                   avgConcentricForce: String
                   avgEccentricForce: String
                   avgConcentricPower: String
                   avgEccentricPower: String
               }
               type HipThrust {
                   idExercise: String
                   repetitionNumber: String
                   maxVelocity: String
                   maxAceleration: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   maxConcentricPower: String
                   maxEccentricPower: String
                   avgVelocity: String
                   avgAceleration: String
                   avgConcentricForce: String
                   avgEccentricForce: String
                   avgConcentricPower: String
                   avgEccentricPower: String
               }
               type ConoTR {
                   idExercise: String
                   repetitionNumber: String
                   maxVelocity: String
                   maxAceleration: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   maxConcentricPower: String
                   maxEccentricPower: String
                   avgVelocity: String
                   avgAceleration: String
                   avgConcentricForce: String
                   avgEccentricForce: String
                   avgConcentricPower: String
                   avgEccentricPower: String
               }
               type PoleaConica {
                   idExercise: String
                   repetitionNumber: String
                   maxVelocity: String
                   maxAceleration: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   maxConcentricPower: String
                   maxEccentricPower: String
                   avgVelocity: String
                   avgAceleration: String
                   avgConcentricForce: String
                   avgEccentricForce: String
                   avgConcentricPower: String
                   avgEccentricPower: String
               }
               type Cone {
                   idExercise: String
                   repetitionNumber: String
                   maxVelocity: String
                   maxAceleration: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   maxConcentricPower: String
                   maxEccentricPower: String
                   avgVelocity: String
                   avgAceleration: String
                   avgConcentricForce: String
                   avgEccentricForce: String
                   avgConcentricPower: String
                   avgEccentricPower: String
               }
               type FreeCharge {
                   idExercise: String
                   repetitionNumber: String
                   maxDistance: String
                   maxVelocity: String
                   maxAceleration: String
                   maxForce: String
                   maxPower: String
                   maxImpulse: String
                   avgDistance: String
                   avgVelocity: String
                   avgPropulsiveVelocity: String
                   avgAceleration: String
                   avgForce: String
                   avgPropulsiveForce: String
                   avgPower: String
                   avgPropulsivePower: String
                   avgImpulse: String
                   timeRelMaxVelocity: String
                   timeRelMaxAceleration: String
                   timeRelMaxForce: String
                   timeRelMaxPower: String
                   timeRelMaxImpulse: String
                   maxRepetition: String
                   percentMaxRepetition: String
               }
               type ComplexFreeCharge {
                   idExercise: String
                   repetitionNumber: String
                   maxDistance: String
                   maxVelocity: String
                   maxAceleration: String
                   maxForce: String
                   maxPower: String
                   maxImpulse: String
                   avgDistance: String
                   avgVelocity: String
                   avgPropulsiveVelocity: String
                   avgAceleration: String
                   avgForce: String
                   avgPropulsiveForce: String
                   avgPower: String
                   avgPropulsivePower: String
                   avgImpulse: String
                   timeRelMaxVelocity: String
                   timeRelMaxAceleration: String
                   timeRelMaxForce: String
                   timeRelMaxPower: String
                   timeRelMaxImpulse: String
                   maxRepetition: String
                   percentMaxRepetition: String
               }
               type IMTP {
                   idExercise: String
                   repetitionNumber: String
                   initialForce: String
                   maxForce: String
                   avgForce: String
                   forceAt50: String
                   forceAt100: String
                   forceAt150: String
                   forceAt200: String
                   forceAt250: String
                   RFDAt50: String
                   RFDAt100: String
                   RFDAt150: String
                   RFDAt250: String
                   timePull: String
                   timeMaxForce: String
                   maxForceTotalTime: String
                   maxForceAsimmetry: String
                   maxLeftForce: String
                   maxRightForce: String
               }
               type RSIDJ {
                   idExercise: String
                   repetitionNumber: String
                   heightJump: String
                   contactTime: String
                   flightTime: String
                   RSI: String
               }
               type Bounce {
                   idExercise: String
                   repetitionNumber: String
                   maxForce: String
                   maxLeftForce: String
                   maxRightForce: String
                   flightTime: String
                   RSI: String
                   contactTime: String
                   heightJump: String
               }
               type CMJ {
                   idExercise: String
                   repetitionNumber: String
                   unweightingTime: String
                   brakingTime: String
                   brakingImpulse: String
                   brakingMaxForceAsymmetry: String
                   brakingMaxForce: String
                   brakingRelativeImpulse: String
                   propulsiveTime: String
                   propulsiveMaxForceAsymmetry: String
                   propulsiveMaxForce: String
                   propulsiveRelativeImpulse: String
                   avgLeftEccentricForce: String
                   avgRightEccentricForce: String
                   flightTime: String
                   landingStiffness: String
                   landingMaxForceAsymmetry: String
                   landingMaxForce: String
                   stabilzationTime : String
                   RSI: String
                   stiffness: String
                   heightJump: String
                   efficiencyIndex: String
                   breakingRFD: String
                   propulsiveRFD: String
                   positiveImpulse: String
                   positiveRelativeImpulse: String
                   propulsiveImpulse: String
               }
               type Squat {
                   idExercise: String
                   repetitionNumber: String
                   concentricAsimmetry: String
                   eccentricAsimmetry: String
                   concentricImpulse: String
                   eccentricImpulse: String
                   maxConcentricForce: String
                   maxEccentricForce: String
                   timeEccentric: String
                   timeConcentric: String
               }
               type Dynamometer {
                   idExercise: String
                   repetitionNumber: String
                   initialForce: String
                   maxForce: String
                   avgForce: String
                   forceAt50: String
                   forceAt100: String
                   forceAt150: String
                   forceAt200: String
                   forceAt250: String
                   RFDAt50: String
                   RFDAt100: String
                   RFDAt150: String
                   RFDAt250: String
                   timePull: String
                   timeMaxForce: String
                   maxForceTotalTime: String
               }
               type HopAndHold {
                   idExercise: String
                   repetitionNumber: String
                   unweightingTime: String
                   brakingTime: String
                   brakingImpulse: String
                   brakingMaxForce: String
                   brakingRelativeImpulse: String
                   propulsiveTime: String
                   propulsiveMaxForce: String
                   propulsiveRelativeImpulse: String
                   avgLeftEccentricForce: String
                   avgRightEccentricForce: String
                   flightTime: String
                   landingStiffness: String
                   landingMaxForce: String
                   stabilzationTime: String
                   RSI: String
                   stiffness: String
               }
               type DinamometroDoble {
                   idExercise: String
                   repetitionNumber: String
                   initialForce: String
                   maxForce: String
                   avgForce: String
                   forceAt50: String
                   forceAt100: String
                   forceAt150: String
                   forceAt200: String
                   forceAt250: String
                   RFDAt50: String
                   RFDAt100: String
                   RFDAt150: String
                   RFDAt250: String
                   timePull: String
                   timeMaxForce: String
                   maxForceTotalTime: String
                   maxForceAsimmetry: String
               }
               type SquatJump {
                   idExercise: String
                   repetitionNumber: String
                   propulsiveTime: String
                   propulsiveMaxForceAsymmetry: String
                   propulsiveMaxForce: String
                   propulsiveRelativeImpulse: String
                   avgLeftEccentricForce: String
                   avgRightEccentricForce: String
                   flightTime: String
                   landingStiffness: String
                   landingMaxForceAsymmetry: String
                   landingMaxForce: String
                   stabilzationTime: String
                   RSI: String
                   heightJump: String
               }

                schema {
                   query: Query
               }
               """

function tap(x)
    println("OBJECT", x)
    println("TYPE", typeof(x))
    println("INSIDE", map(z -> println(typeof(z)), x))
    return x
end

const GenericQueryCSV =
    exerciseType ->
        property ->
            (root, args, ctx, info) ->
                (return queryCSV(args["id"], exerciseType)[!, property|>String])
macro QueryDict(exerciseKey, fieldBlock)
    fields = fieldBlock.args |> tap |> y -> filter(x -> typeof(x) === Symbol, y)
    String(exerciseKey) =>
        map(y -> String(y) => String(y) |> (exerciseKey |> GenericQueryCSV), fields) |> Dict
end

macro listQuery(prop, list)
    String(prop) => (root, args, ctx, info) -> (return Dict(String(prop) => list))
end
const resolvers = Dict(
    "Query" => Dict(
        "exercises" =>
            (root, args, ctx, info) -> (
                return Dict(
                    "remoprensa" => "[]",
                    "yoyo" => "[]",
                    "sillon_camilla" => "[]",
                    "empuje_frontal" => "[]",
                    "imtp" => "[]",
                    "cmj" => "[]",
                    "freecharge" => "[]",
                    "complexfreecharge" => "[]",
                    "cone" => "[]",
                    "rsidj" => "[]",
                )
            ),
    ),
    "Exercises" => Dict(
        @listQuery remoprensa "[idExercise,repetitionNumber,maxVelocity,maxAceleration,maxConcentricForce,maxEccentricForce,maxConcentricPower,maxEccentricPower,avgVelocity,avgAceleration,avgConcentricForce,avgEccentricForce,avgConcentricPower,avgEccentricPower]",
        @listQuery yoyo "[idExercise,repetitionNumber,maxVelocity,maxAceleration,maxConcentricForce,maxEccentricForce,maxConcentricPower,maxEccentricPower,avgVelocity,avgAceleration,avgConcentricForce,avgEccentricForce,avgConcentricPower,avgEccentricPower]",
        @listQuery sillon_camilla "[idExercise,repetitionNumber,maxVelocity,maxAceleration,maxConcentricForce,maxEccentricForce,maxConcentricPower,maxEccentricPower,avgVelocity,avgAceleration,avgConcentricForce,avgEccentricForce,avgConcentricPower,avgEccentricPower]",
        @listQuery empuje_frontal "[idExercise,repetitionNumber,maxVelocity,maxAceleration,maxConcentricForce,maxEccentricForce,maxConcentricPower,maxEccentricPower,avgVelocity,avgAceleration,avgConcentricForce,avgEccentricForce,avgConcentricPower,avgEccentricPower]",
        @listQuery hip_thrust "[idExercise,repetitionNumber,maxVelocity,maxAceleration,maxConcentricForce,maxEccentricForce,maxConcentricPower,maxEccentricPower,avgVelocity,avgAceleration,avgConcentricForce,avgEccentricForce,avgConcentricPower,avgEccentricPower]",
        @listQuery cone_tr "[idExercise,repetitionNumber,maxVelocity,maxAceleration,maxConcentricForce,maxEccentricForce,maxConcentricPower,maxEccentricPower,avgVelocity,avgAceleration,avgConcentricForce,avgEccentricForce,avgConcentricPower,avgEccentricPower]",
        @listQuery polea_conica "[idExercise,repetitionNumber,maxVelocity,maxAceleration,maxConcentricForce,maxEccentricForce,maxConcentricPower,maxEccentricPower,avgVelocity,avgAceleration,avgConcentricForce,avgEccentricForce,avgConcentricPower,avgEccentricPower]",
        @listQuery cone "[idExercise,repetitionNumber,maxVelocity,maxAceleration,maxConcentricForce,maxEccentricForce,maxConcentricPower,maxEccentricPower,avgVelocity,avgAceleration,avgConcentricForce,avgEccentricForce,avgConcentricPower,avgEccentricPower]",
        @listQuery freecharge "[idExercise,repetitionNumber,maxDistance,maxVelocity,maxAceleration,maxForce,maxPower,maxImpulse,avgDistance,avgVelocity,avgPropulsiveVelocity,avgAceleration,avgForce,avgPropulsiveForce,avgPower,avgPropulsivePower,avgImpulse,timeRelMaxVelocity,timeRelMaxAceleration,timeRelMaxForce,timeRelMaxPower,timeRelMaxImpulse,maxRepetition,percentMaxRepetition]",
        @listQuery complexfreecharge "[idExercise,repetitionNumber,maxDistance,maxVelocity,maxAceleration,maxForce,maxPower,maxImpulse,avgDistance,avgVelocity,avgPropulsiveVelocity,avgAceleration,avgForce,avgPropulsiveForce,avgPower,avgPropulsivePower,avgImpulse,timeRelMaxVelocity,timeRelMaxAceleration,timeRelMaxForce,timeRelMaxPower,timeRelMaxImpulse,maxRepetition,percentMaxRepetition]",
        @listQuery imtp "[idExercise,repetitionNumber,initialForce,maxForce,avgForce,forceAt50,forceAt100,forceAt150,forceAt200,forceAt250,RFDAt50,RFDAt100,RFDAt150,RFDAt250,timePull,timeMaxForce,maxForceTotalTime]",
        @listQuery rsidj "[idExercise,repetitionNumber,heightJump,contactTime,flightTime,RSI]",
        @listQuery salto_continuo "[idExercise,repetitionNumber,maxForce,maxLeftForce,maxRightForce,flightTime,RSI,contactTime,heightJump]",
        @listQuery cmj "[idExercise,repetitionNumber,unweightingTime,brakingTime,brakingImpulse,brakingMaxForceAsymmetry,brakingMaxForce,brakingRelativeImpulse,propulsiveTime,propulsiveMaxForceAsymmetry,propulsiveMaxForce,propulsiveRelativeImpulse,avgLeftEccentricForce,avgRightEccentricForce,flightTime,landingStiffness,landingMaxForceAsymmetry,landingMaxForce,stabilzationTime,RSI,stiffness,heightJump,efficiencyIndex,breakingRFD,propulsiveRFD,positiveImpulse,positiveRelativeImpulse,propulsiveImpulse]",
        @listQuery squat "[idExercise,repetitionNumber,concentricAsimmetry,eccentricAsimmetry,concentricImpulse,eccentricImpulse,maxConcentricForce,maxEccentricForce,timeEccentric,timeConcentric]",
        @listQuery dynamometer "[idExercise,repetitionNumber,initialForce,maxForce,avgForce,forceAt50,forceAt100,forceAt150,forceAt200,forceAt250,RFDAt50,RFDAt100,RFDAt150,RFDAt250,timePull,timeMaxForce,maxForceTotalTime]",
        @listQuery hht "[idExercise,repetitionNumber,unweightingTime,brakingTime,brakingImpulse,brakingMaxForce,brakingRelativeImpulse,propulsiveTime,propulsiveMaxForce,propulsiveRelativeImpulse,avgLeftEccentricForce,avgRightEccentricForce,flightTime,landingStiffness,landingMaxForce,stabilzationTime,RSI,stiffness]",
        @listQuery double_dynamometer "[idExercise,repetitionNumber,initialForce,maxForce,avgForce,forceAt50,forceAt100,forceAt150,forceAt200,forceAt250,RFDAt50,RFDAt100,RFDAt150,RFDAt250,timePull,timeMaxForce,maxForceTotalTime,maxForceAsimmetry]",
        @listQuery squat_jump "[idExercise,repetitionNumber,propulsiveTime,propulsiveMaxForceAsymmetry,propulsiveMaxForce,propulsiveRelativeImpulse,avgLeftEccentricForce,avgRightEccentricForce,flightTime,landingStiffness,landingMaxForceAsymmetry,landingMaxForce,stabilzationTime,RSI,heightJump]"
    ),
    (@QueryDict RemoPrensa begin
        idExercise
        repetitionNumber
        maxVelocity
        maxAceleration
        maxConcentricForce
        maxEccentricForce
        maxConcentricPower
        maxEccentricPower
        avgVelocity
        avgAceleration
        avgConcentricForce
        avgEccentricForce
        avgConcentricPower
        avgEccentricPower
    end),
    (@QueryDict Yoyo begin
        idExercise
        repetitionNumber
        maxVelocity
        maxAceleration
        maxConcentricForce
        maxEccentricForce
        maxConcentricPower
        maxEccentricPower
        avgVelocity
        avgAceleration
        avgConcentricForce
        avgEccentricForce
        avgConcentricPower
        avgEccentricPower
    end),
    (@QueryDict SillonCamilla begin
        idExercise
        repetitionNumber
        maxVelocity
        maxAceleration
        maxConcentricForce
        maxEccentricForce
        maxConcentricPower
        maxEccentricPower
        avgVelocity
        avgAceleration
        avgConcentricForce
        avgEccentricForce
        avgConcentricPower
        avgEccentricPower
    end),
    (@QueryDict EmpujeFrontal begin
        idExercise
        repetitionNumber
        maxVelocity
        maxAceleration
        maxConcentricForce
        maxEccentricForce
        maxConcentricPower
        maxEccentricPower
        avgVelocity
        avgAceleration
        avgConcentricForce
        avgEccentricForce
        avgConcentricPower
        avgEccentricPower
    end),
    (@QueryDict HipThrust begin
        idExercise
        repetitionNumber
        maxVelocity
        maxAceleration
        maxConcentricForce
        maxEccentricForce
        maxConcentricPower
        maxEccentricPower
        avgVelocity
        avgAceleration
        avgConcentricForce
        avgEccentricForce
        avgConcentricPower
        avgEccentricPower
    end),
    (@QueryDict ConoTR begin
        idExercise
        repetitionNumber
        maxVelocity
        maxAceleration
        maxConcentricForce
        maxEccentricForce
        maxConcentricPower
        maxEccentricPower
        avgVelocity
        avgAceleration
        avgConcentricForce
        avgEccentricForce
        avgConcentricPower
        avgEccentricPower
    end),
    (@QueryDict PoleaConica begin
        idExercise
        repetitionNumber
        maxVelocity
        maxAceleration
        maxConcentricForce
        maxEccentricForce
        maxConcentricPower
        maxEccentricPower
        avgVelocity
        avgAceleration
        avgConcentricForce
        avgEccentricForce
        avgConcentricPower
        avgEccentricPower
    end),
    (@QueryDict IMTP begin
        idExercise
        repetitionNumber
        initialForce
        maxForce
        avgForce
        forceAt50
        forceAt100
        forceAt150
        forceAt200
        forceAt250
        RFDAt50
        RFDAt100
        RFDAt150
        RFDAt250
        timePull
        timeMaxForce
        maxForceTotalTime
        maxForceAsimmetry
        maxLeftForce
        maxRightForce
    end),
    (@QueryDict CMJ begin
        idExercise
        repetitionNumber
        unweightingTime
        brakingTime
        brakingImpulse
        brakingMaxForceAsymmetry
        brakingMaxForce
        brakingRelativeImpulse
        propulsiveTime
        propulsiveMaxForceAsymmetry
        propulsiveMaxForce
        propulsiveRelativeImpulse
        avgLeftEccentricForce
        avgRightEccentricForce
        flightTime
        landingStiffness
        landingMaxForceAsymmetry
        landingMaxForce
        stabilzationTime
        RSI
        stiffness
        heightJump
        efficiencyIndex
        breakingRFD
        propulsiveRFD
        positiveImpulse
        positiveRelativeImpulse
        propulsiveImpulse
    end),
    (@QueryDict Dinamometro begin
        idExercise
        repetitionNumber
        initialForce
        maxForce
        avgForce
        forceAt50
        forceAt100
        forceAt150
        forceAt200
        forceAt250
        RFDAt50
        RFDAt100
        RFDAt150
        RFDAt250
        timePull
        timeMaxForce
        maxForceTotalTime
    end),
    (@QueryDict FreeCharge begin
        idExercise
        repetitionNumber
        maxDistance
        maxVelocity
        maxAceleration
        maxForce
        maxPower
        maxImpulse
        avgDistance
        avgVelocity
        avgPropulsiveVelocity
        avgAceleration
        avgForce
        avgPropulsiveForce
        avgPower
        avgPropulsivePower
        avgImpulse
        timeRelMaxVelocity
        timeRelMaxAceleration
        timeRelMaxForce
        timeRelMaxPower
        timeRelMaxImpulse
        maxRepetition
        percentMaxRepetition
    end),
    (@QueryDict ComplexFreeCharge begin
        idExercise
        repetitionNumber
        maxDistance
        maxVelocity
        maxAceleration
        maxForce
        maxPower
        maxImpulse
        avgDistance
        avgVelocity
        avgPropulsiveVelocity
        avgAceleration
        avgForce
        avgPropulsiveForce
        avgPower
        avgPropulsivePower
        avgImpulse
        timeRelMaxVelocity
        timeRelMaxAceleration
        timeRelMaxForce
        timeRelMaxPower
        timeRelMaxImpulse
        maxRepetition
        percentMaxRepetition
    end),
    (@QueryDict Cone begin
        idExercise
        repetitionNumber
        maxVelocity
        maxAceleration
        maxConcentricForce
        maxEccentricForce
        maxConcentricPower
        maxEccentricPower
        avgVelocity
        avgAceleration
        avgConcentricForce
        avgEccentricForce
        avgConcentricPower
        avgEccentricPower
    end),
    (@QueryDict RSIDJ begin
        idExercise
        repetitionNumber
        heightJump
        contactTime
        flightTime
        RSI
    end),
    (@QueryDict SquatJump begin
        idExercise
        repetitionNumber
        propulsiveTime
        propulsiveMaxForceAsymmetry
        propulsiveMaxForce
        propulsiveRelativeImpulse
        avgLeftEccentricForce
        avgRightEccentricForce
        flightTime
        landingStiffness
        landingMaxForceAsymmetry
        landingMaxForce
        stabilzationTime
        RSI
        heightJump
    end),
    (@QueryDict SaltoContinuo begin
        idExercise
        repetitionNumber
        maxForce
        maxLeftForce
        maxRightForce
        flightTime
        RSI
        contactTime
        heightJump
    end),
    (@QueryDict Squat begin
        idExercise
        repetitionNumber
        concentricAsimmetry
        eccentricAsimmetry
        concentricImpulse
        eccentricImpulse
        maxConcentricForce
        maxEccentricForce
        timeEccentric
        timeConcentric
    end),
    (@QueryDict HopAndHold begin
        idExercise
        repetitionNumber
        unweightingTime
        brakingTime
        brakingImpulse
        brakingMaxForce
        brakingRelativeImpulse
        propulsiveTime
        propulsiveMaxForce
        propulsiveRelativeImpulse
        avgLeftEccentricForce
        avgRightEccentricForce
        flightTime
        landingStiffness
        landingMaxForce
        stabilzationTime
        RSI
        stiffness
    end),
    (@QueryDict DinamometroDoble begin
        idExercise
        repetitionNumber
        initialForce
        maxForce
        avgForce
        forceAt50
        forceAt100
        forceAt150
        forceAt200
        forceAt250
        RFDAt50
        RFDAt100
        RFDAt150
        RFDAt250
        timePull
        timeMaxForce
        maxForceTotalTime
        maxForceAsimmetry
    end)
)

const mySchema = Schema(schema, resolvers)

Threads.@spawn Get(
    "/graphql",
    (request, HTTP) -> begin
        request.body |>
        s ->
            JSON.parse(s::AbstractString; dicttype=Dict, inttype=Int64) |>
            query ->
                mySchema.execute(query["query"]) |> result -> HTTP.Response(200, result)
    end,
)

Threads.@spawn Get(
    "/options",
    (request, HTTP) -> begin
        HTTP.Response(200, "{\"options\":$(exerciseOptions)}")
    end,
)

Threads.@spawn Get(
    "/:exercise/:userID",
    (request, HTTP) -> begin
        try
            (request.params["userID"], request.params["exercise"]) |>
            requestParams ->
                (parse(Int64, requestParams[1]), requestParams[2] |> Symbol) |>
                y ->
                    queryCSV(y...) |>
                    objecttable |>
                    JSON_RESPONSE -> HTTP.Response(200, JSON_RESPONSE)
        catch e
            HTTP.Response(
                404,
                "" *
                request.params["userID"] *
                " doesn't have any exercises of type" *
                request.params["exercise"],
            )
        end
    end,
)

start(host="127.0.0.1", port=8086, verbose=true)
