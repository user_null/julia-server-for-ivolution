using DataFrames, CSV, Query, PlotlyJS
using Dash

user = CSV.File("users.csv")
exercises = CSV.File("exercises.csv")
athletes = CSV.File("athletes.csv")
cmj = CSV.File("exerciserepetitionsplatformcmj.csv")

userData = user |> @map({_.idUser, _.email}) |> DataFrame
athletesData =
    athletes |>
    @map({
        _.idUser,
        _.nameAthlete,
        _.idAthlete,
        _.idAthleteGlobal,
        _.idAthleteGroup,
    }) |>
    DataFrame
exercisesData =
    exercises |>
    @map({_.idUser, _.idExercise, _.idExerciseType, _.idAthlete}) |>
    DataFrame

app = dash()
p = plot(
    table(
        header = attr(
            values = names(df),
            fill_collor = "paleturquoise",
            align = "left",
        ),
        cells = attr(
            values = [df.Rank, df.State, df.Postal, df.Population],
            fill_color = "lavender",
            align = "left",
        ),
    ),
)
app.layout = html_div() do
    html_h1("Hello Dash"),
    html_div("Dash: A web application framework for your data."),
    dcc_graph(
        figure = p,
        style = Dict("height" => 300),
        id = "my-graph",
    )
end

run_server(app, "0.0.0.0", debug = true);
